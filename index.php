<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Coq & Tonic :: Clucking Soon Landing Page</title>
	<meta name="description" content="Coq & Tonic :: Digital Marketing">
	<meta name="author" content="Coq & Tonic">
	<link rel="stylesheet" href="css/core.css">
</head>
<body>
	<div class="img-container">
		<img class="img-fullwidth" src="/img/landing.jpg" />
	</div>
	<br />
	<div class="img-container">
		<div class="img-social-twitter">
			&nbsp;
		</div>
		<div class="img-social-mail">
			&nbsp;
		</div>
	</div>
</body>
</html>